import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Login from "../views/login";
import Home from "../views/home";

const Routes = () => {
  const loggedIn = localStorage.getItem("loggedIn")
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" render={() => <Login />} />
        {  loggedIn ? 
        <Route exact path="/home" render={() => <Home />} /> : <Redirect push to="/" />
        }
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;