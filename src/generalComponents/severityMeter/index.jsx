import React, { useState, useEffect} from "react";
import styled from "styled-components";

// SM = SeverityMeter
function SeverityMeter({ severity }) {
  const [severityLevel, setSeverityLevel] = useState(0)

  useEffect(() => {
    const calculateSeverity =  () => {
      if (severity > 150) {
        setSeverityLevel(100)
      } else {
        setSeverityLevel(severity * 100 / 150)
      }
    } 
    calculateSeverity();
  }, [severity]);

  


  return (
    <SMContainer>
      <SMBoxLight></SMBoxLight>
      <SMBoxMedium></SMBoxMedium>
      <SMBoxHard></SMBoxHard>
    <SMBar severity={severityLevel}></SMBar>
    </SMContainer>
  );
}

export default SeverityMeter;

// Styles of the component

const SMContainer = styled.div`
position: relative;
  width: 150px;
  display: flex;
  align-items: center;
  margin: 0  1rem
`;

const SMBoxLight = styled.p`
  background-color: #334789;
  width: 33%;
  height:16px;
`;

const SMBoxMedium = styled.div`
  background-color: #ffec6a;
  width: 33%;
  height:16px;
`;

const SMBoxHard = styled.div`
  background-color: #e51d23;
  width: 33%;
  height:16px;
`;

const SMBar = styled.div`
background-color: black;
width: ${(props) => (props.severity ? `${props.severity}%` : "0%" )};
height: 0.3rem;
position: absolute;
`;