import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faChevronDown,
	faChevronUp,
	faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';
import Loader from "react-loader-spinner";

function Box({
	title,
	width,
	displayExpand,
	expand,
	setExpand,
	children,
	close,
	setAlert,
	list,
	loading,
}) {
	return (
		<BoxContainer width={width} list={list}>
			<BoxTitleContainer>
				<BoxTitle>{title}</BoxTitle>
				{expand && displayExpand ? (
					<FontAwesomeIcon
						icon={faChevronUp}
						onClick={(_) => {
							setExpand(false);
						}}
						className="BoxExpandIcon"
					/>
				) : !expand && displayExpand ? (
					loading ? (
						<LoaderContainer><Loader type="TailSpin" color="#000" height={15} width={15} /></LoaderContainer>
					) : (
						<FontAwesomeIcon
							icon={faChevronDown}
							onClick={(_) => {
								setExpand(true);
							}}
							className="BoxExpandIcon"
						/>
					)
				) : (
					close && (
						<FontAwesomeIcon
							onClick={(_) => {
								setAlert(false);
							}}
							icon={faTimesCircle}
							className="BoxCloseIcon"
						/>
					)
				)}
			</BoxTitleContainer>
			{children}
		</BoxContainer>
	);
}

export default Box;

Box.defaultProps = {
	expand: false,
	close: false,
};

// Styles of the component

const BoxContainer = styled.div`
	width: 80vw;
	display: flex;
	flex-flow: column;
	max-height: ${(props) => (props.list ? '200px' : 'fit-content')};
	margin: 2rem 4rem;
	background: rgb(245, 245, 245);
	border: 1px solid #bbbbbb;
	@media (min-width: 900px) {
		width: ${(props) => (props.width ? props.width : '390px')};
	}
`;

const LoaderContainer = styled.div`
margin-right: 1rem;
`;

const BoxTitleContainer = styled.div`
	width: 100%;
	height: 30px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	background-color: #bbbbbb;
`;

const BoxTitle = styled.p`
	margin-left: 0.5rem;
`;
