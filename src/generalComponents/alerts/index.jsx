import React, { useState} from "react";
import styled from "styled-components";
import Box from "../containerBox";
import Resolve from "./components/Resolve";
import Escalate from "./components/Escalate";

function Alert({ type, setAlert, children, ticketInfo }) {
    const [ step, setStep] = useState(1);

  return (
      <>
        {type === "resolve" ? (
            <AlertContainer>
              <Box title="Action" width="550px">
                <Resolve step={step} setStep={setStep} setAlert={setAlert} ticketInfo={ticketInfo}/>
              </Box>
          </AlertContainer>
        ) : type === "escalate" ? (
            <AlertContainer>
              <Box title="Action" width="550px">
                <Escalate step={step} setStep={setStep} setAlert={setAlert} ticketInfo={ticketInfo}/>
              </Box>
          </AlertContainer>
         ) : (
          <AlertContainer>
            <Box title={type} width="550px" close={true} setAlert={ setAlert }>
              {children}
            </Box>
        </AlertContainer>
      )
        }
      </>
    
  );
}

export default Alert;

// Styles of the component
const AlertContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background: rgba(255, 255, 255, 0.5);
  display: flex;
  align-items: ${(props) => (props.position=== "start" ? "flex-start" : "center")};
  justify-content: center;
`;
