import React, { useState } from "react";
import styled from "styled-components";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

function Resolve({ step, setStep, setAlert, ticketInfo }) {
  const [check, setCheck] = useState(false);
  const [callerEmail, setCallerEmail] = useState("");
  const [comment, setComment] = useState("");
  const resolveTicket = async () => {
    const token = localStorage.getItem("token");
    const data = {
      callerName: ticketInfo.callerName,
      customerName: ticketInfo.customerName,
      location: ticketInfo.location,
      department: ticketInfo.department,
      region: "",
      equipment: ticketInfo.equipment,
      story: ticketInfo.story,
      status: 1,
      category: [],
      driverName: ticketInfo.driverName,
      time: ticketInfo.eventTime,
      saveDAte: ticketInfo.saveDAte,
      escalatedUser: "",
      priority: "",
      callerEmail,
      comment,
      id: ticketInfo.id,
      ticketId: ticketInfo.id,
      sendAnEmail: check
    };
    const header = { headers: { Authorization: `Bearer ${token}` } };

    await axios
      .post(`/api/email/send`, data, header)
      .then((response) => {
        setStep(step + 1);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      {step === 1 ? (
        <>
          <ResolveOrganizer>
            <ResolveFirstColumn>
              <ResolveEmailSection>
                <p>Caller's Email </p>
                <ResolveInput
                  type="text"
                  spellCheck={false}
                  placeholder=""
                  value={callerEmail}
                  onChange={(e) => {
                    setCallerEmail(e.target.value);
                  }}
                />
              </ResolveEmailSection>
              <ResolveCheckboxContainer>
                <span>Send email to caller</span>
                <ResolveCheckboxHidden
                  checked={check}
                  onChange={() => setCheck(!check)}
                />
                <ResolveCheckboxStyled
                  checked={check}
                  onChange={() => setCheck(!check)}
                  onClick={() => setCheck(!check)}
                >
                  <FontAwesomeIcon
                    icon={faCheck}
                    className="ResolveCheckboxIcon"
                  />
                </ResolveCheckboxStyled>
              </ResolveCheckboxContainer>
            </ResolveFirstColumn>
            <ResolveSecondColumn>
              <ResolveCommentSection>
                <p>Comments</p>
              </ResolveCommentSection>
              <ResolveComments
                value={comment}
                onChange={(e) => {
                  setComment(e.target.value);
                }}
              />
            </ResolveSecondColumn>
          </ResolveOrganizer>
          <UnresolvedButtonsContainer>
            <ResolveSubmitButton
              onClick={(e) => {
                resolveTicket();
              }}
            >
              Submit
            </ResolveSubmitButton>
            <ResolveCancel
              onClick={(_) => {
                setAlert(false);
              }}
            >
              Cancel
            </ResolveCancel>
          </UnresolvedButtonsContainer>{" "}
        </>
      ) : (
        <ResolvedContainer>
          <p>The enquiry is resolved successfully</p>
          <ResolvedButton
            onClick={(_) => {
              setAlert(false);
            }}
          >
            Ok
          </ResolvedButton>
        </ResolvedContainer>
      )}
    </>
  );
}

export default Resolve;

// Styles of the component STEP#1

const ResolveOrganizer = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  padding: 0.5rem 2rem;
  text-align: left;
`;

const ResolveFirstColumn = styled.div`
  width: 55%;
  display: flex;
  flex-flow: column;
  justify-content: center;
`;

const ResolveSecondColumn = styled.div`
  width: 45%;
  display: flex;
  flex-flow: column;
  justify-content: center;
`;

const ResolveEmailSection = styled.div`
  display: flex;
  align-items: center;
`;

const ResolveInput = styled.input`
  width: 150px;
  height: 2rem;
  margin: 1rem auto;
  padding: 0 0.5rem;
  border: 1px solid #888888;
  color: #888888;
  &:focus {
    outline: none;
  }
`;

const ResolveCommentSection = styled.div`
  display: flex;
  padding: 1.5rem 0;
`;

const ResolveComments = styled.textarea`
  width: 170px;
  height: 8rem;
  margin: 0;
  padding: 1rem;
  border: 1px solid #888888;
  color: #888888;
  resize: none;
  &:focus {
    outline: none;
  }
`;

const ResolveCheckboxContainer = styled.div`
  width: 220px;
  margin: 2rem 0 0.5rem 1px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const ResolveCheckboxHidden = styled.input`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const ResolveCheckboxStyled = styled.div`
  display: inline-block;
  width: 16px;
  height: 16px;
  text-align: center;
  margin-left: 1.5rem;
  background: ${(props) => (props.checked ? "#1d72e1" : "transparent")};
  border: 1px solid #bbbbbb;
  border-radius: 3px;
  transition: all 150ms;
  cursor: pointer;
`;

const UnresolvedButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ResolveSubmitButton = styled.button`
  width: fit-content;
  height: 2rem;
  margin: 1rem;
  cursor: pointer;
  padding: 0 1rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  color: green;
  background: transparent;
  &:focus {
    outline: none;
  }
`;

const ResolveCancel = styled.p`
  cursor: pointer;
  margin: 0 1rem;
  color: red;
`;

// Styles of the component STEP#2

const ResolvedContainer = styled.div`
  width: 100%;
  height: 270px;
  display: flex;
  flex-flow: column;
  justify-content: space-evenly;
  align-items: center;
`;

const ResolvedButton = styled.button`
  width: fit-content;
  height: 2rem;
  cursor: pointer;
  padding: 0 2rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;