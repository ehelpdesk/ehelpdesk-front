import React, { useState } from "react";
import axios from "axios";
import Alert from "../index";
import styled from "styled-components";

function Unresolved({ unresolvedTickets, setUnresolvedAlert }) {

  const [ resolve, setResolve] = useState(false);

  const reminder = async () => {
    const token = localStorage.getItem("token");
    const data = { id: localStorage.getItem("userId"), name: localStorage.getItem("userName"), remindNextTime: true, remindSetDate: "" };
    const header = { headers: { "Content-Type":"application/x-www-form-urlencoded", Authorization: `Bearer ${token}`}};

    await axios
      .post(`/api/remind/new`, data, header)
      .then((response) => {
        setUnresolvedAlert(false)
      })
      .catch((error) => {
        console.log(error)
      });
  };


  return (
    <>
      <UnresolvedTicketsContainer>
        {/* {unresolvedTickets.map((ticket, index) => {
          return (
            <UnresolvedTicket style={{ display: "flex" }} key={"UT" + index}>
              <UnresolvedTicketFirstColumn>
                <p>Ticket #{ticket.id}</p>
              </UnresolvedTicketFirstColumn>
              <UnresolvedTicketSecondColumn>
                {ticket.status === "1" ? (
                  <ResolvedIcon viewBox="0 0 24 24">
                    <polyline points="20 6 9 17 4 12" />
                  </ResolvedIcon>
                ) : (
                  <UnresolvedButton onClick={ _=> {setResolve(true)}} >View</UnresolvedButton>
                )}
              </UnresolvedTicketSecondColumn>
            </UnresolvedTicket>
          );
        })} */}
      </UnresolvedTicketsContainer>
      <UnresolvedButtonsContainer>
        <UnresolvedButton onClick={ _=> {setUnresolvedAlert(false)}}>Ok</UnresolvedButton>
        <UnresolvedButton onClick={ _=> {reminder()}}>Remind Me later</UnresolvedButton>
      </UnresolvedButtonsContainer>
      { resolve && <Alert type="resolve" setAlert={setResolve}/>}
      </>
  );
}

export default Unresolved;

// Styles of the component
const UnresolvedTicketsContainer = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;
  align-items: center;
  padding: 0.5rem 2rem;
  text-align: left;
`;

const UnresolvedTicket = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const UnresolvedTicketFirstColumn = styled.div`
  width: 20%;
  display: flex;
  justify-content: center;
`;

const UnresolvedTicketSecondColumn = styled.div`
  width: 50%;
  display: flex;
  justify-content: center;
`;

const ResolvedIcon = styled.svg`
  fill: none;
  stroke: white;
  stroke-width: 3px;
  background-color: #1d72e1;
  border-radius: 10px;
  padding: 1px;
  height: 20px;
`;

const UnresolvedButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const UnresolvedButton = styled.button`
  width: fit-content;
  height: 2rem;
  margin: 1rem;
  cursor: pointer;
  padding: 0 1rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;