import React, { useState } from "react";
import axios from "axios";
import styled from "styled-components";

function Escalate({ step, setStep, setAlert, ticketInfo }) {
  const [callerEmail, setCallerEmail] = useState("");
  const [comment, setComment] = useState("");
  const [ priority, setPriority] = useState("low");

  const escalateTicket = async () => {
    const token = localStorage.getItem("token");
    const data = {
      callerName: ticketInfo.callerName,
      customerName: ticketInfo.customerName,
      location: ticketInfo.location,
      department: ticketInfo.department,
      region: "",
      equipment: ticketInfo.equipment,
      story: ticketInfo.story,
      status: 2,
      category: [],
      driverName: ticketInfo.driverName,
      time: ticketInfo.eventTime,
      saveDAte: ticketInfo.saveDAte,
      escalatedUser: "",
      priority,
      callerEmail,
      comment,
      id: ticketInfo.id,
      ticketId: ticketInfo.id,
      sendAnEmail: true
    };
    const header = { headers: { Authorization: `Bearer ${token}` } };

    await axios
      .post(`/api/email/send`, data, header)
      .then((response) => {
        setStep(step + 1);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      {step === 1 ? (
        <>
          <EscalateOrganizer>
            <EscalateFirstColumn>
              <EscalateSection>
                <EscalateText>Escalate</EscalateText>
                <EscalateInput>
                  <option value="users">Users</option>
                </EscalateInput>
              </EscalateSection>
              <EscalateSection>
                <EscalateText>Priority</EscalateText>
                <EscalateInput onChange= {e => { setPriority(e.target.value)}}>
                  <option value="low">Low</option>
                  <option value="normal">Normal</option>
                  <option value="high">High</option>
                  <option value="urgent">Urgent</option>
                </EscalateInput>
              </EscalateSection>
            </EscalateFirstColumn>
            <EscalateSecondColumn>
              <EscalateSection>
                <EscalateText2>Caller's Email </EscalateText2>
                <EscalateTextInput
                  type="email"
                  spellCheck={false}
                  placeholder=""
                  onChange= {e => { setCallerEmail(e.target.value)}}
                />
              </EscalateSection>
              <EscalateCommentSection>
                <p>Comments</p>
              </EscalateCommentSection>
              <EscalateComments onChange= {e => { setComment(e.target.value)}}/>
            </EscalateSecondColumn>
          </EscalateOrganizer>
          <EscalatedButtonsContainer>
            <EscalateSubmitButton
               onClick={(e) => {
                escalateTicket();
              }}
            >
              Submit
            </EscalateSubmitButton>
            <EscalateCancel onClick={ _=> {setAlert(false)}}>Cancel</EscalateCancel>
          </EscalatedButtonsContainer>{" "}
        </>
      ) : (
        <EscalatedContainer>
          <EscalateConfirmation >
            <EscalateIcon viewBox="0 0 24 24">
              <polyline points="20 6 9 17 4 12" />
            </EscalateIcon>
            <EscalateConfirmationTitle>
              Email was sent
            </EscalateConfirmationTitle>
          </EscalateConfirmation>
          <EscalateConfirmationText>
            Email with all the details was sent to the support team for further
            investigation
          </EscalateConfirmationText>
          <EscalatedButton onClick={ _=> {setAlert(false)}}>Ok</EscalatedButton>
        </EscalatedContainer>
      )}
    </>
  );
}

export default Escalate;

// Styles of the component STEP#1

const EscalateOrganizer = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  padding: 0.5rem 0.5rem;
  text-align: left;
`;

const EscalateFirstColumn = styled.div`
  width: 50%;
  display: flex;
  flex-flow: column;
  justify-content: center;
  margin-left: 1rem;
`;

const EscalateSecondColumn = styled.div`
  width: 50%;
  display: flex;
  flex-flow: column;
  justify-content: center;
  margin-right: 1rem;
`;

const EscalateSection = styled.div`
  display: flex;
  align-items: center;
`;

const EscalateText = styled.p`
  min-width: 55px;
`;

const EscalateText2 = styled.p`
  min-width: 100px;
`;

const EscalateTextInput = styled.input`
  width: 50%;
  height: 2rem;
  margin: 1rem 0rem;
  padding: 0 0.5rem;
  border: 1px solid #888888;
  border-radius: 5px;
  background-color: transparent;
  color: #888888;
  &:focus {
    outline: none;
  }
`;

const EscalateInput = styled.select`
  width: 50%;
  height: 2rem;
  margin: 1rem 1rem;
  padding: 0 0.5rem;
  border: 1px solid #888888;
  border-radius: 5px;
  background-color: transparent;
  &:focus {
    outline: none;
  }
`;

const EscalateCommentSection = styled.div`
  display: flex;
  padding: 1.5rem 0;
`;

const EscalateComments = styled.textarea`
  width: 80%;
  height: 20%;
  margin: 0;
  padding: 1rem;
  background-color: transparent;
  border: 1px solid #888888;
  color: #888888;
  resize: none;
  &:focus {
    outline: none;
  }
`;

const EscalatedButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const EscalateSubmitButton = styled.button`
  width: fit-content;
  height: 2rem;
  margin: 1rem;
  cursor: pointer;
  padding: 0 1rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  color: green;
  background: transparent;
  &:focus {
    outline: none;
  }
`;

const EscalateCancel = styled.p`
  cursor: pointer;
  margin: 0 1rem;
  color: red;
`;

// Styles of the component STEP#2

const EscalatedContainer = styled.div`
  width: 100%;
  height: 270px;
  display: flex;
  flex-flow: column;
  justify-content: space-evenly;
  align-items: center;
`;

const EscalatedButton = styled.button`
  width: fit-content;
  height: 2rem;
  cursor: pointer;
  padding: 0 2rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;

const EscalateConfirmation = styled.div`
  display: flex;
  align-items: center;
`;

const EscalateConfirmationTitle = styled.p`
  font-size: 18px;
  margin-left: 1rem
`;

const EscalateIcon = styled.svg`
  fill: none;
  stroke: green;
  stroke-width: 2px;
  background-color: transparent;
  border: solid 3px green;
  border-radius: 50px;
  padding: 1px;
  height: 32px;
  weight: 32px;
`;

const EscalateConfirmationText = styled.p`
  width: 62%;
  text-align: center;
`;
