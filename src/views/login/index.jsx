import React, { useState, useEffect } from "react";
import axios from "axios";
import styled from "styled-components";
import Loader from "react-loader-spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUnlockAlt } from "@fortawesome/free-solid-svg-icons";
import { Redirect } from "react-router-dom";

function Login() {
  const [rememberMe, setRememberMe] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    rememberLogin();
  }, []);

  const handleSubmit = async () => {
    setLoading(true);
    const data = { email, password };
    const header = {};
    await axios
      .post(`/api/auth/sign/in`, data, header)
      .then((response) => {
        setLoading(false);
        if (rememberMe) {
          localStorage.setItem("user", email);
          localStorage.setItem("password", password);
          localStorage.setItem("rememberMe", true);
          localStorage.setItem("loggedIn", true)
        }
        setRedirect(true);
        localStorage.setItem("token", response.data.token);
        localStorage.setItem("userName", response.data.firstname);
        localStorage.setItem("userId", response.data.id);
        localStorage.setItem("loggedIn", true)
      })
      .catch((error) => {
        setLoading(false);
        setError(true);
      });
  };

  const rememberLogin = async () => {
    const loadData = localStorage.getItem("rememberMe") === "true";
    if (loadData) {
      setEmail(localStorage.getItem("user"));
      setPassword(localStorage.getItem("password"));
    } else {
      setEmail("");
      setPassword("");
    }
  };

  return (
    <LoginContainer>
      {loading && (
        <LoaderContainer><Loader type="TailSpin" color="#888888" height={50} width={50} /></LoaderContainer>
      )}
      <LoginBox>
        <LoginTitle>Welcome to CIG E-Helpdesk</LoginTitle>
        <p>Login</p>
        <LoginInput
          value={email}
          type="email"
          spellCheck={false}
          placeholder="Email"
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
        <LoginInputPasswordContainer>
          <LoginInputPassword
            value={password}
            type="password"
            minLength="6"
            maxLength="15"
            placeholder="Password"
            onFocus={(e) => (e.target.placeholder = "")}
            onBlur={(e) => (e.target.placeholder = "Password")}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
          <FontAwesomeIcon icon={faUnlockAlt} className="LoginInputIcon" />
        </LoginInputPasswordContainer>

        <LoginCheckboxContainer>
          <LoginCheckboxHidden
            checked={rememberMe}
            onChange={() => {
              setRememberMe(!rememberMe);
            }}
          />
          <LoginCheckboxStyled
            checked={rememberMe}
            onClick={() => {
              setRememberMe(!rememberMe);
            }}
            onChange={() => {
              setRememberMe(!rememberMe);
            }}
          >
            <LoginCheckboxIcon viewBox="0 0 24 24">
              <polyline points="20 6 9 17 4 12" />
            </LoginCheckboxIcon>
          </LoginCheckboxStyled>

          <span>Remember Me</span>
        </LoginCheckboxContainer>

        <LoginButton
          onClick={(_) => {
            handleSubmit();
          }}
        >
          Login
        </LoginButton>
        {error && (
          <ErrorMessage>Invalid Email & Password. Try Again</ErrorMessage>
        )}
        <ForgotPassword>Forgot Password</ForgotPassword>
      </LoginBox>
      {redirect && <Redirect push to="/home" />}
    </LoginContainer>
  );
}

export default Login;

// Styles of the component

const LoginContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const LoaderContainer = styled.div`
  width: 300vw;
  height: 350vh;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgba(255, 255, 255, 0.5);
`;

const LoginBox = styled.div`
  width: 300px;
  height: 350px;
  display: flex;
  flex-flow: column;
  align-items: center;
  border: 1px solid #888888;
`;

const LoginTitle = styled.h1`
  font-size: 16px;
  margin: 2rem auto;
  width: 122px;
  text-align: center;
`;

const LoginInput = styled.input`
  width: 200px;
  height: 2rem;
  margin: 1rem auto;
  padding: 0 0.5rem;
  border: 1px solid #888888;
  color: #888888;
  &:focus {
    outline: none;
  }
`;

const LoginInputPasswordContainer = styled.div`
  display: flex;
  align-items: center;
`;

const LoginInputPassword = styled.input`
  width: 180px;
  height: 2rem;
  margin: 1rem auto 1rem;
  padding: 0 0.5rem 0 2rem;
  border: 1px solid #888888;
  color: #101010;
  &:focus {
    outline: none;
    font-size: 26px;
    letter-spacing: 2px;
  }
  &:placeholder: {
    display: none;
  }
  &:blur {
    font-size: 26px;
    letter-spacing: 2px;
  }
`;

const LoginCheckboxContainer = styled.div`
  width: 220px;
  margin-bottom: 0.5rem;
  margin-left: 1px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const LoginCheckboxHidden = styled.input`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const LoginCheckboxStyled = styled.div`
  display: inline-block;
  width: 16px;
  height: 16px;
  margin-right: 0.5rem;
  background: ${(props) => (props.checked ? "#101010" : "transparent")};
  border: 1px solid #bbbbbb;
  border-radius: 3px;
  transition: all 150ms;
  cursor: pointer;
`;

const LoginCheckboxIcon = styled.svg`
  fill: none;
  stroke: white;
  stroke-width: 3px;
`;

const LoginButton = styled.button`
  width: 220px;
  height: 2rem;
  margin: 0.5rem 0 0rem 0;
  cursor: pointer;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;

const ErrorMessage = styled.p`
  position: relative;
  color: red;
  font-size: 10px;
  margin-top: 0.2rem;
`;

const ForgotPassword = styled.p`
  margin-top: 1rem;
`;