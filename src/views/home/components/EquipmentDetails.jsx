import React from "react";
import Box from "../../../generalComponents/containerBox";
import styled from "styled-components";

// ED = Equipment Details
function EquipmentDetails({ expand, setExpand, equipmentInfo }) {
  
  const equipmentDetails = [
    { identifier: "GMTP ID", information: equipmentInfo.gmtp_id },
    { identifier: "Vehicle ID", information: equipmentInfo.hire_no },
    { identifier: "Serial NO", information: equipmentInfo.serial_no },
    { identifier: "Model", information: equipmentInfo.model },
    { identifier: "On Hire", information: equipmentInfo.hire ? "Yes" : "No" },
    { identifier: "Special Use", information: equipmentInfo.special_use ? "Yes" : "No" },
    { identifier: "VOR", information: equipmentInfo.vor ? "Yes" : "No" },
    { identifier: "Product Type", information: equipmentInfo.product_type },
    { identifier: "SIM Card NO", information: equipmentInfo.simcard_no },
    { identifier: "SIM Card Supplier", information: equipmentInfo.simcard_supplier },
    { identifier: "Battery ID", information: equipmentInfo.battery_id },
    { identifier: "Battery State of Charge", information: "35%" },
    { identifier: "Modem Version", information: equipmentInfo.modem_version },
    {
      identifier: "Firmware Version",
      information: equipmentInfo.firmware_ver,
    },
    { identifier: "Expansion Module Version", information: equipmentInfo.exp_mod_ver },
    {
      identifier: "Expansion Module Module ID",
      information: equipmentInfo.exp_mod_id,
    },
  ];

  return (
    <Box
      title="Equipment Details"
      width="50%"
      expand={expand}
      displayExpand={true}
      setExpand={setExpand}
    >
      {expand === true && (
        <EDColumn>
          <EDDataSection>
            {equipmentDetails.map((detail, index) => {
              return (
                <EDDataDetails key={"ED" + index}>
                  <EDDataIdentifier>{detail.identifier}</EDDataIdentifier>
                  <EDDataIdentifier>{detail.information}</EDDataIdentifier>
                </EDDataDetails>
              );
            })}
          </EDDataSection>
        </EDColumn>
      )}
    </Box>
  );
}

export default EquipmentDetails;

// FROM HERE JUST STYLES FOR THE COMPONENT

const EDColumn = styled.div`
  display: flex;
  margin: 1rem 2rem;
`;

const EDDataSection = styled.div`
  display: flex;
  flex-flow: column;
  align-items: flex-start;
  padding: 0.5rem 0;
`;

const EDDataDetails = styled.div`
  display: flex;
`;

const EDDataIdentifier = styled.p`
  min-width: 250px;
  padding: 0.2rem 0;
`;
