import React, { useEffect, useState } from "react";
import axios from "axios";
import Box from "../../../generalComponents/containerBox";
import styled from "styled-components";

// DH = DriverHistory
function DriverHistory({ expand, setExpand, equipmentID }) {
  const [driversHistory, setDriversHistory] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getDriverHistory(equipmentID);
  }, [equipmentID]);

  const getDriverHistory = async (equipmentID) => {
    setLoading(true)
    const token = localStorage.getItem("token");

    await axios
      .get(`/api/driver/history/${equipmentID}`, {
        equipmentID,
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setLoading(false)
        setDriversHistory(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <Box
      title="Driver History"
      width="50%"
      expand={expand}
      displayExpand={true}
      setExpand={setExpand}
      loading={loading}
    >
      {expand === true && (
        <>
          <div style={{ position: "relative" }}>
            <div style={{ maxHeight: "302px", overflow: "auto" }}>
              <DHTable border="1" cellSpacing="0" cellPadding="0">
                <thead>
                  <tr>
                    <DHSmallColumn>#</DHSmallColumn>
                    <DHMediumColumn>Name</DHMediumColumn>
                    <DHBigColumn>Time</DHBigColumn>
                    <DHMediumColumn>Card ID</DHMediumColumn>
                    <DHMediumColumn>Wiegand</DHMediumColumn>
                    <DHMediumColumn>Authorized Access</DHMediumColumn>
                  </tr>
                </thead>
                <tbody>
                  {driversHistory.map((row, index) => {
                    return (
                      <tr key={index}>
                        <DHInfoCell>{index + 1}</DHInfoCell>
                        <DHInfoCell>{row.name}</DHInfoCell>
                        <DHInfoCell>
                          {<DHCalendar type="text" disabled value={row.time} />}
                        </DHInfoCell>
                        <DHInfoCell>{row.cardId}</DHInfoCell>
                        <DHInfoCell>{row.wiegand}</DHInfoCell>
                        <DHInfoCell>{row.authorized ? "Yes" : "No"}</DHInfoCell>
                      </tr>
                    );
                  })}
                </tbody>
              </DHTable>
            </div>
          </div>
        </>
      )}
    </Box>
  );
}

export default DriverHistory;

// FROM HERE JUST STYLES FOR THE COMPONENT

const DHTable = styled.table`
  width: 100%;
`;

const DHSmallColumn = styled.th`
  width: 55px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const DHMediumColumn = styled.th`
  width: 100px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const DHBigColumn = styled.th`
  width: fit-content;
  padding: 1rem 0;
  font-weight: normal;
  border: 1px solid #888888;
`;

const DHInfoCell = styled.td`
  padding: 1rem 0;
  max-width: 100px;
  border: 1px solid #888888;
  text-align: center;
`;

const DHCalendar = styled.input`
  width: 80%;
  padding-left: 1rem;
  border: 1px solid #888888;
  color: #888888;
  &:focus {
    outline: none;
  }
`;

