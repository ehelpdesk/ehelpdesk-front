import React, { useState, useEffect } from "react";
import Box from "../../../generalComponents/containerBox";
import SeverityMeter from "../../../generalComponents/severityMeter";
import styled from "styled-components";
import axios from "axios";

// EV = Event
function Event({ expand, setExpand, categories, equipmentID, customer }) {
  const [impacts, setImpacts] = useState([]);
  const [preops, setPreops] = useState([]);
  const [sessions, setSessions] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    console.log(categories, equipmentID, customer)
    const setEventData = async () => {
      setLoading(true)
      const token = localStorage.getItem("token");
  
      await axios
        .get(`/api/home/${equipmentID}`, {
          params: {
            customer,
          },
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => {
          setImpacts(response.data.impact);
          setPreops(response.data.preop);
          setSessions(response.data.session);
          setLoading(false)
        })
        .catch((error) => {
  
          console.log(error);
        });
    };
    setEventData();
  }, [categories, customer, equipmentID]);

  return (
    <Box
      title="Event"
      width="50%"
      expand={expand}
      displayExpand={true}
      setExpand={setExpand}
      loading={loading}
    >
      {expand === true && (
        <>
          {categories.impacts && (
            <>
              <EVColumn>
                <EVColumnTitle>Impacts</EVColumnTitle>
                <EVImpactDataList>
                  {impacts.map((impact, index) => {
                    return (
                      <EVListContainer key={"EV" + index}>
                        <li>Time: {impact.impact_time}</li>
                        <li>Impact value: {impact.impact_value}</li>
                        <li>Impact Level: {impact.severityLvl}</li>
                        <div style={{ display: "flex" }}>
                          <li>Severity Level: </li>
                          <SeverityMeter severity={impact.severityLvl} />
                        </div>
                        <li>Driver: {impact.driver_name}</li>
                        <li>Unlock Driver: {impact.unlock_driver}</li>
                        <li>Unlock time: {impact.unlock_driver}</li>
                      </EVListContainer>
                    );
                  })}
                </EVImpactDataList>
              </EVColumn>
            </>
          )}
          {categories.preop && (
            <>
              <EVColumn>
                <EVColumnTitle>Preop Checks</EVColumnTitle>
                <EVImpactDataList>
                  {preops.map((preop, index) => {
                    return (
                      <EVListContainer key={"EV" + index}>
                        <li>
                          Critical Failed Questions:{" "}
                          {preop.preopList[0] && preop.preopList[0].question}
                        </li>
                        <li>Time: {preop.answered_time}</li>
                        <li>Duration: {preop.duration}</li>
                        <li>Driver: {preop.driver_name}</li>
                        <li>Unlock Supervisor: --</li>
                        <li>Unlock Time: --</li>
                      </EVListContainer>
                    );
                  })}
                </EVImpactDataList>
              </EVColumn>
            </>
          )}
          {categories.sessions && (
            <>
              <EVColumn>
                <EVColumnTitle>Sessions</EVColumnTitle>
                <EVImpactDataList>
                  {sessions.map((session, index) => {
                    return (
                      <EVListContainer key={"EV" + index}>
                        <li>Start: {session.startTime}</li>
                        <li>End: {session.endTime}</li>
                        <li>During: {session.duration}</li>
                        <li>Driver: {session.driver}</li>
                        <li>Seal: {session.seat}</li>
                        <li>Hydraulic: {session.hydr}</li>
                        <li>Tracking: {session.track}</li>
                      </EVListContainer>
                    );
                  })}
                </EVImpactDataList>
              </EVColumn>
            </>
          )}
        </>
      )}
    </Box>
  );
}

export default Event;

// FROM HERE JUST STYLES FOR THE COMPONENT

const EVColumn = styled.div`
  width: 50%;
`;

const EVColumnTitle = styled.p`
  padding: 1rem;
`;

const EVImpactDataList = styled.ul`
  margin-left: 3rem;
`;

const EVListContainer = styled.div`
  margin-bottom: 3rem;
`;

const EVButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-top: -1rem;
`;

const EVButton = styled.button`
  width: fit-content;
  height: 2rem;
  margin: 0 1rem 1rem 1rem;
  cursor: pointer;
  padding: 0 1rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;