import React, { useEffect, useState} from "react";
import Box from "../../../generalComponents/containerBox";
import axios from "axios";
import styled from "styled-components";

// SD = Site Details
function SiteDetails({ expand, setExpand, customer}) {
  const [generalDetails, setGeneralDetails] = useState([]);
  const [loading, setLoading] = useState(false);
  
  useEffect(() => {
    const getSiteDetails = async () => {
      setLoading(true)
      const token = localStorage.getItem("token");
      await axios
  
        .get(`/api/home/site/details`, {
          params: {customer},
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => { 
          setLoading(false)
         setGeneralDetails(response.data)
        })
        .catch((error) => {
          console.log(error)
        });
    };
    getSiteDetails();
  }, [customer]);

  return (
    <Box
      title="Site Details"
      width="auto"
      expand={expand}
      displayExpand={true}
      setExpand={setExpand}
      loading={loading}
    >
      {expand === true && (
        <SDColumn>
          <SDDataSection>
            <SDDataIdentifier>Total Drivers:</SDDataIdentifier>
            <SDDataColumn>
              <p>{generalDetails.totalDrivers}</p>
              <p>{generalDetails.expiredLicence} with expired license</p>
              <p>{generalDetails.noLicenceDetails} without license details</p>
              <p>{generalDetails.driverNotActive} not active in the past week</p>
            </SDDataColumn>
          </SDDataSection>
          <SDDataSection>
            <SDDataIdentifier>Total Vehicles:</SDDataIdentifier>
            <SDDataColumn>
              <p>{generalDetails.totalVehicles}</p>
              <p>{generalDetails.vehNotActive} inactive for over 72 hours</p>
              <p>{generalDetails.vehInUsed} used in last 24 hours</p>
            </SDDataColumn>
          </SDDataSection>
        </SDColumn>
      )}
    </Box>
  );
}

export default SiteDetails;

// FROM HERE JUST STYLES FOR THE COMPONENT

const SDColumn = styled.div`
  display: flex;
  flex-flow: column;
  margin: 2rem;
`;

const SDDataSection = styled.div`
  display: flex;
  align-items: flex-start;
  padding: 0.5rem 0;
`;

const SDDataIdentifier = styled.p`
  min-width: 150px;
`;

const SDDataColumn = styled.div`
  display: flex;
  flex-flow: column;
  align-items: flex-start;
`;
