import React, { useEffect, useState } from "react";
import axios from "axios";
import Box from "../../../generalComponents/containerBox";
import styled from "styled-components";

// TL = Timeline
function Timeline({ expand, setExpand, equipmentID }) {
  const [timeline, setTimeline] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [finalPage, setFinalPage] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getTimeline = async () => {
      setLoading(true)
      const token = localStorage.getItem("token");
      await axios
        .get(`/api/home/timeline/${equipmentID}`, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => {
          setLoading(false)
          setTimeline(response.data);
          setFinalPage(response.data.length);
        })
        .catch((error) => {
          console.log(error);
        });
    };
    getTimeline();
  }, [equipmentID]);

  return (
    <Box
      title="Timeline"
      width="auto"
      expand={expand}
      displayExpand={true}
      setExpand={setExpand}
      loading={loading}
    >
      {expand === true && (
        <>
          <TLTable border="1" cellSpacing="0" cellPadding="0">
            <thead>
              <tr>
                <TLNumberColumn>#</TLNumberColumn>
                <TLColumn>Time</TLColumn>
                <TLColumn>Event</TLColumn>
                <TLColumn>Driver</TLColumn>
              </tr>
            </thead>
            {timeline
              .filter((timeline) => timeline.id === currentPage)
              .map((row, index) => {
                let number = 0;
                return (
                  <tbody key={`TL${index}`}>
                    {row.cardSwipe && (
                      <tr>
                        <TLInfoCell>{number = number + 1}</TLInfoCell>
                        <TLInfoCell>
                          <TLCalendar>{row.cardSwipe}</TLCalendar>
                        </TLInfoCell>
                        <TLInfoCell>Card Swipe</TLInfoCell>
                        <TLInfoCell>{row.driver}</TLInfoCell>
                      </tr>
                    )}
                    {row.sessionStart && (
                      <tr>
                        <TLInfoCell>{number = number + 1}</TLInfoCell>
                        <TLInfoCell>
                          <TLCalendar>{row.sessionStart}</TLCalendar>
                        </TLInfoCell>
                        <TLInfoCell>Session Start</TLInfoCell>
                        <TLInfoCell>{row.driver}</TLInfoCell>
                      </tr>
                    )}
                    {row.preopStart && (
                      <tr>
                        <TLInfoCell>{number = number + 1}</TLInfoCell>
                        <TLInfoCell>
                          <TLCalendar>{row.preopStart}</TLCalendar>
                        </TLInfoCell>
                        <TLInfoCell>Preop Start</TLInfoCell>
                        <TLInfoCell>{row.driver}</TLInfoCell>
                      </tr>
                    )}
                    {row.preopEnd && (
                      <tr>
                        <TLInfoCell>{number = number + 1}</TLInfoCell>
                        <TLInfoCell>
                          <TLCalendar>{row.preopEnd}</TLCalendar>
                        </TLInfoCell>
                        <TLInfoCell>Preop End</TLInfoCell>
                        <TLInfoCell>{row.driver}</TLInfoCell>
                      </tr>
                    )}
                    {row.lockoutType && (
                      <tr>
                        <TLInfoCell>{number = number + 1}</TLInfoCell>
                        <TLInfoCell>
                          <TLCalendar>{row.lockoutType}</TLCalendar>
                        </TLInfoCell>
                        <TLInfoCell>Lockout type</TLInfoCell>
                        <TLInfoCell>{row.driver}</TLInfoCell>
                      </tr>
                    )}
                    {row.sessionEnd && (
                      <tr>
                        <TLInfoCell>{number = number + 1}</TLInfoCell>
                        <TLInfoCell>
                          <TLCalendar>{row.sessionEnd}</TLCalendar>
                        </TLInfoCell>
                        <TLInfoCell>Session End</TLInfoCell>
                        <TLInfoCell>{row.driver}</TLInfoCell>
                      </tr>
                    )}
                  </tbody>
                );
              })}
          </TLTable>
          <TLButtonsContainer>
            <TLButton
              onClick={(_) => {
                setCurrentPage(currentPage - 1);
              }}
              disabled={currentPage === 1 && true}
            >
              Previous
            </TLButton>
            <TLButton
              onClick={(_) => {
                setCurrentPage(currentPage + 1);
              }}
              disabled={currentPage === finalPage && true}
            >
              Next
            </TLButton>
          </TLButtonsContainer>
        </>
      )}
    </Box>
  );
}

export default Timeline;

// FROM HERE JUST STYLES FOR THE COMPONENT

const TLTable = styled.table`
  width: 100%;
`;

const TLNumberColumn = styled.th`
  width: 100px;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const TLColumn = styled.th`
  padding: 1rem 0;
  font-weight: normal;
  border: 1px solid #888888;
`;

const TLInfoCell = styled.td`
  padding: 1rem 0;
  max-width: 100px;
  border: 1px solid #888888;
  text-align: center;
`;

const TLCalendar = styled.div`
  display: flex;
  justify-content: center;
  width: 90%;
  margin: 0 auto;
  border: 1px solid #888888;
  color: #888888;
  &:focus {
    outline: none;
  }
`;

const TLButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const TLButton = styled.button`
  width: fit-content;
  height: 2rem;
  margin: 1rem;
  cursor: pointer;
  padding: 0 1rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;
