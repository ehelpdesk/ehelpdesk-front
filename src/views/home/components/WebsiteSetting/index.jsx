import React, { useState, useEffect } from "react";
import axios from "axios";
import Box from "../../../../generalComponents/containerBox";
import Alert from "../../../../generalComponents/alerts";
import MasterCodes from "./utilsModals/MasterCodes";
import DriversList from "./utilsModals/DriversList";
import LicenseExpired from "./utilsModals/ExpiredLicense";
import DriversBlackList from "./utilsModals/DriversBlacklist";
import styled from "styled-components";

// WS = Website Settings
function WebsiteSettings({ expand, setExpand, equipmentID }) {
  const [alert, setAlert] = useState(false);
  const [alertType, setAlertType] = useState("");
  const [websiteSettings, setWebsiteSettings] = useState([]);
  const [masterCodes, setMasterCodes] = useState([]);
  const [allDrivers, setAllDrivers] = useState([]);
  const [blackList, setBlackList] = useState([]);
  const [expiredLicenses, setExpiredLicenses] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getWebsiteSettings(equipmentID);
    getMasterCodes(equipmentID);
    getAllDrivers(equipmentID);
    getBlackList(equipmentID);
    getExpiredLicenses(equipmentID);
  }, [equipmentID]);

  const getWebsiteSettings = async (equipmentID) => {
    setLoading(true)
    const token = localStorage.getItem("token");

    await axios
      .get(
        `/api/home/website/settings/${equipmentID}`,
        {
          equipmentID,
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => {
        setLoading(false)
        setWebsiteSettings([
          {
            identifier: "Red Impact Threshold",
            information: response.data.red_impact_threshold,
          },
          {
            identifier: "Impact Recalibration Date ",
            information: response.data.impact_recalibration_date,
          },
          {
            identifier: "Impact calibration percentage ",
            information: response.data.impact_cal_perc,
          },
          {
            identifier: "Impact Lockout Enabled ",
            information: response.data.imp_lock_enabled,
          },
          {
            identifier: "Idle Timeout(s)",
            information: response.data.idle_timeout,
          },
          {
            identifier: "Survey Timeout(s)",
            information: response.data.survey_timeout,
          },
          {
            identifier: "Can-Rules Loaded",
            information: response.data.canrule,
          },
          {
            identifier: "Preop Check Schedule",
            information: response.data.preop_schedule,
          },
          {
            identifier: "Canhours/Acculated Key Hours",
            information: response.data.key_hours,
          },
          { identifier: "Service Status", information: "" },
        ]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getMasterCodes = async (equipmentID) => {
    const token = localStorage.getItem("token");

    await axios
      .get(`/api/master/list/${equipmentID}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setMasterCodes(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllDrivers = async (equipmentID) => {
    const token = localStorage.getItem("token");

    await axios
      .get(`/api/driver/history/${equipmentID}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setAllDrivers(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getBlackList = async (equipmentID) => {
    const token = localStorage.getItem("token");

    await axios
      .get(
        `/api/driver/list/black-list/${equipmentID}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => {
        setBlackList(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getExpiredLicenses = async (equipmentID) => {
    const token = localStorage.getItem("token");

    await axios
      .get(
        `/api/driver/list/expired-lic/${equipmentID}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => {
        setExpiredLicenses(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const utils = [
    {
      title: "Master Codes",
      drivers: [
        masterCodes[0] && masterCodes[0].master_name,
        masterCodes[1] && masterCodes[1].master_name,
        masterCodes[2] && masterCodes[2].master_name,
      ],
    },
    {
      title: "Drivers List",
      drivers: [
        allDrivers[0] && allDrivers[0].name,
        allDrivers[1] && allDrivers[1].name,
        allDrivers[2] && allDrivers[2].name,
      ],
    },
    {
      title: "Drivers on Blacklist",
      drivers: [
        blackList[0] && blackList[0].driver_name,
        blackList[1] && blackList[1].driver_name,
        blackList[2] && blackList[2].driver_name,
      ],
    },
    {
      title: "Drivers have license expired",
      drivers: [
        expiredLicenses[0] && expiredLicenses[0].driver_name,
        expiredLicenses[1] && expiredLicenses[1].driver_name,
        expiredLicenses[2] && expiredLicenses[2].driver_name,
      ],
    },
  ];

  const displayMoreInfo = (type) => {
    setAlertType(type);
    setAlert(true);
  };

  return (
    <>
      {alert && (
        <Alert type={alertType} setAlert={setAlert}>
          {alertType === "Master Codes" ? (
            <MasterCodes masterCodes={masterCodes} />
          ) : alertType === "Drivers List" ? (
            <DriversList allDrivers={allDrivers} />
          ) : alertType === "Drivers on Blacklist" ? (
            <DriversBlackList blackList={blackList} />
          ) : (
            alertType === "Drivers have license expired" && (
              <LicenseExpired expiredLicenses={expiredLicenses} />
            )
          )}
        </Alert>
      )}
      <Box
        title="Website Settings"
        width="50%"
        expand={expand}
        displayExpand={true}
        setExpand={setExpand}
        loading={loading}
      >
        {expand === true && (
          <>
            <WSColumn>
              <WSDataSection>
                {websiteSettings.map((detail, index) => {
                  return (
                    <WSDataDetails key={"WS" + index}>
                      <WSDataIdentifier>{detail.identifier}</WSDataIdentifier>
                      <WSDataIdentifier>{detail.information}</WSDataIdentifier>
                    </WSDataDetails>
                  );
                })}
              </WSDataSection>
            </WSColumn>
            <WSColumn>
              <WSUtilsSection>
                {utils.map((util, index) => {
                  return (
                    <WSDataDetails key={"WSUtils" + index}>
                      <div>
                        <WSDataIdentifier>{util.title}</WSDataIdentifier>
                        <WSDataIdentifier>
                          {util.drivers[0]}
                          {util.drivers[1] && <>, {util.drivers[1]} </>}...
                        </WSDataIdentifier>
                      </div>
                      <WSShowMoreUtilsContainer
                        onClick={(_) => {
                          displayMoreInfo(util.title);
                        }}
                      >
                        (<WSShowMoreUtils>Show More</WSShowMoreUtils>)
                      </WSShowMoreUtilsContainer>
                    </WSDataDetails>
                  );
                })}
              </WSUtilsSection>
            </WSColumn>
          </>
        )}
      </Box>
    </>
  );
}

export default WebsiteSettings;

// FROM HERE JUST STYLES FOR THE COMPONENT

const WSColumn = styled.div`
  display: flex;
  margin: 1rem 2rem;
`;

const WSDataSection = styled.div`
  display: flex;
  flex-flow: column;
  align-items: flex-start;
  padding: 0.5rem 0;
`;

const WSUtilsSection = styled.div`
  display: flex;
  flex-flow: column;
  align-items: flex-start;
  justify-content: center;
  padding: 0.5rem 0;
`;

const WSDataDetails = styled.div`
  display: flex;
`;

const WSDataIdentifier = styled.p`
  min-width: 250px;
  padding: 0.2rem 0;
`;

const WSShowMoreUtilsContainer = styled.p`
  align-self: center;
  cursor: pointer;
`;

const WSShowMoreUtils = styled.span`
  color: #89cff0;
`;