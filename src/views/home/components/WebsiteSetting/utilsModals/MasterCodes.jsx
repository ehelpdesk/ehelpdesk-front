import React from "react";
import styled from "styled-components";

// MC = DriverHistory
function MasterCodes({ masterCodes }) {
  return (
    <MCTableContainer>
      <MCTable border="1" cellSpacing="0" cellPadding="0">
        <thead>
          <tr>
            <MCMediumColumn>Name</MCMediumColumn>
            <MCMediumColumn>Card PIN</MCMediumColumn>
          </tr>
        </thead>
        {masterCodes.map((row, index) => {
          return (
            <tbody key={"MC" + index}>
              <tr>
                <MCInfoCell>{row.master_name}</MCInfoCell>
                <MCInfoCell>{row.weigand}</MCInfoCell>
              </tr>
            </tbody>
          );
        })}
      </MCTable>
    </MCTableContainer>
  );
}

export default MasterCodes;

// Styles of the component
const MCTableContainer = styled.div`
  width: 100%;
  max-height: 277px;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

const MCTable = styled.table`
  width: 100%;
  height: 100%;
`;

const MCMediumColumn = styled.th`
  width: 100px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const MCInfoCell = styled.td`
  padding: 1rem 0;
  max-width: 100px;
  border: 1px solid #888888;
  text-align: center;
`;
