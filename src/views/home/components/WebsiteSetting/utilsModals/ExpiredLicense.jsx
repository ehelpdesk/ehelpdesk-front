import React from "react";
import styled from "styled-components";

// LE = License Expired
function LicenseExpired({ expiredLicenses }) {
  return (
    <LETableContainer>
      <LETable border="1" cellSpacing="0" cellPadding="0">
        <thead>
          <LESmallColumn>#</LESmallColumn>
          <LEMediumColumn>Name</LEMediumColumn>
        </thead>
        {expiredLicenses.map((row, index) => {
          return (
            <tbody>
              <LEInfoCell>{index + 1}</LEInfoCell>
              <LEInfoCell>{row.driver_name}</LEInfoCell>
            </tbody>
          );
        })}
      </LETable>
    </LETableContainer>
  );
}

export default LicenseExpired;

// Styles of the component
const LETableContainer = styled.div`
  width: 100%;
  max-height: 277px;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

const LETable = styled.table`
  width: 100%;
`;

const LEMediumColumn = styled.th`
  width: 100px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const LESmallColumn = styled.th`
  width: 20px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const LEInfoCell = styled.td`
  padding: 1rem 0;
  max-width: 100px;
  border: 1px solid #888888;
  text-align: center;
`;
