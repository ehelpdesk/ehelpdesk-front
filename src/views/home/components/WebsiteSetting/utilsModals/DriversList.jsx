import React from "react";
import styled from "styled-components";

// DL = DriverHistory
function DriverList({ allDrivers }) {
  return (
    <DLTableContainer>
      <DLTable border="1" cellSpacing="0" cellPadding="0">
        <thead>
          <DLMediumColumn>Name</DLMediumColumn>
          <DLMediumColumn>Card PIN</DLMediumColumn>
        </thead>
        {allDrivers.map((row, index) => {
          return (
            <tbody>
              <DLInfoCell>{row.name}</DLInfoCell>
              <DLInfoCell>{row.cardId}</DLInfoCell>
            </tbody>
          );
        })}
      </DLTable>
    </DLTableContainer>
  );
}

export default DriverList;

// Styles of the component
const DLTableContainer = styled.div`
  width: 100%;
  max-height: 277px;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

const DLTable = styled.table`
  width: 100%;
`;

const DLMediumColumn = styled.th`
  width: 100px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const DLInfoCell = styled.td`
  padding: 1rem 0;
  max-width: 100px;
  border: 1px solid #888888;
  text-align: center;
`;
