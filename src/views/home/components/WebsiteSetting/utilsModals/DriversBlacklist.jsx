import React from "react";
import styled from "styled-components";

// DBL = BlackList
function BlackList({ blackList }) {
  return (
    <BLTableContainer>
      <BLTable border="1" cellSpacing="0" cellPadding="0">
        <thead>
          <BLNumberColumn>#</BLNumberColumn>
          <BLMediumColumn>Name</BLMediumColumn>
          <BLSmallColumn>Card ID</BLSmallColumn>
          <BLSmallColumn>Pin №</BLSmallColumn>
        </thead>
        {blackList.map((row, index) => {
          return (
            <tbody>
              <BLInfoCell>{index + 1}</BLInfoCell>
              <BLInfoCell>{row.driver_name}</BLInfoCell>
              <BLInfoCell>{row.weigand}</BLInfoCell>
              <BLInfoCell>{row.user_cd}</BLInfoCell>
            </tbody>
          );
        })}
      </BLTable>
    </BLTableContainer>
  );
}

export default BlackList;

// Styles of the component
const BLTableContainer = styled.div`
  width: 100%;
  max-height: 277px;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

const BLTable = styled.table`
  width: 100%;
`;

const BLMediumColumn = styled.th`
  width: 100px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const BLSmallColumn = styled.th`
  width: 50px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const BLNumberColumn = styled.th`
  width: 20px;
  font-weight: normal;
  padding: 1rem 0;
  border: 1px solid #888888;
`;

const BLInfoCell = styled.td`
  padding: 1rem 0;
  max-width: 100px;
  border: 1px solid #888888;
  text-align: center;
`;