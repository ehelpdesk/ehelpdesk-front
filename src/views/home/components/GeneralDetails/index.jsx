import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import Alert from '../../../../generalComponents/alerts';
import Box from '../../../../generalComponents/containerBox';
import AllDrivers from './components/allDrivers/index';
import axios from 'axios';
import styled from 'styled-components';

// GD = General Details
function GeneralDetails({
  customer,
  customerName,
  equipmentIDK,
  equipment,
  equipmentInfo,
  displaySiteDetails,
  displayEquipmentDetails,
  displayTimeline,
  displayDriverHistory,
  showDriverHistory,
  displayEvent,
  eventCategories,
  setEventCategories,
  displayWebsiteSettings,
  setSearch,
  search,
  ticketInfo,
  clearForm,
  setClearForm,
}) {
  const [driverHistory, setDriverHistory] = useState(false);
  const [impacts, setImpacts] = useState(false);
  const [preop, setPreop] = useState(false);
  const [sessions, setSessions] = useState(false);
  const [wrongCallerName, setWrongCallerName] = useState(false);
  const [wrongCustomerName, setWrongCustomerName] = useState(false);
  const [wrongEquipmentID, setWrongEquipmentID] = useState(false);
  const [displayInput, setDisplayInput] = useState(false);
  const [allDrivers, setAllDrivers] = useState([]);
  const [alert, setAlert] = useState(false);
  const [checkCategory, setCheckCategory] = useState(false);
  const [callerName, setCallerName] = useState('');
  const [selectedDriver, setSelectedDriver] = useState('');
  const [eventTime, setEventTime] = useState('');
  const [story, setStory] = useState('');
  const [location, setLocation] = useState('');
  const [department, setDepartment] = useState('');
  const [saveError, setSaveError] = useState(false);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [filteredSuggestions, setFilteredSuggestions] = useState([]);
  const [list, setList] = useState([]);
  const [autocomplete, setAutocomplete] = useState(false);
  const [region, setRegion] = useState('-');
  const [hasDrivers, setHastDrivers] = useState(false);
  const [savedTicket, setSavedTicket] = useState(false);
  
  useEffect(() => {
    const getEquipmentData = async (equipmentID) => {
      const token = localStorage.getItem('token');
      await axios
        .get(`/api/equipment/${equipmentID}`, {
          equipmentID,
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((response) => {
          equipmentInfo(response.data);
          getAllDrivers(equipmentID);
          displayEquipmentDetails(true);
          displayTimeline(true);
          displayWebsiteSettings(true);
        })
        .catch((error) => {
          setWrongEquipmentID(true);
        });
    };

    const changeGeneral = () => {
      if (ticketInfo !== 'default' && ticketInfo !== null) {
        setCallerName(ticketInfo.callerName);
        customer(ticketInfo.customerName);
        displaySiteDetails(true);
        equipmentIDK(ticketInfo.equipment);
        setLocation(ticketInfo.location);
        setDepartment(ticketInfo.department);
        setStory(ticketInfo.story);
        setSelectedDriver(ticketInfo.driverName);
        setEventTime(ticketInfo.eventTime);
        getAllDrivers(ticketInfo.equipment);
        getEquipmentData(ticketInfo.equipment);
        setWrongEquipmentID(false);
        setWrongCustomerName(false);
        setWrongCallerName(false);
        setDriverHistory(false);
        setImpacts(false);
        setPreop(false);
        setSessions(false);
        setSearch(false);
      } else {
        setCallerName('');
        customer('');
        equipmentIDK('');
        setLocation('');
        setDepartment('');
        setStory('');
        setSelectedDriver('');
        setEventTime('');
        setDriverHistory(false);
        setImpacts(false);
        setPreop(false);
        setSessions(false);
        setSearch(false);
        setDisplayInput(false);
      }

      if (clearForm === true) {
        setCallerName('');
        customer('');
        equipmentIDK('');
        setLocation('');
        setDepartment('');
        setStory('');
        setSelectedDriver('');
        setEventTime('');
        setClearForm(false);
        setWrongEquipmentID(false);
        setWrongCustomerName(false);
        setWrongCallerName(false);
        setDriverHistory(false);
        setImpacts(false);
        setPreop(false);
        setSessions(false);
        setSearch(false);
        setRegion('-');
        setDisplayInput(false);
      }
    };

    changeGeneral();
  }, [
    ticketInfo,
    displayEvent,
    clearForm,
    customer,
    displaySiteDetails,
    equipmentIDK,
    setClearForm,
    setSearch,
    displayWebsiteSettings,
    displayTimeline,
    displayEquipmentDetails,
    equipmentInfo,
  ]);

  useEffect(() => {
    const createList = () => {
      let preview = [];

      hasDrivers &&
        equipment !== '' &&
        allDrivers.map((suggestion) => {
          return preview.push(suggestion.driver_name);
        });
      setList(preview);
    };
    createList();
  }, [allDrivers, equipment, hasDrivers]);

  const getEquipmentData = async (equipmentID) => {
    const token = localStorage.getItem('token');
    await axios
      .get(`/api/equipment/${equipmentID}`, {
        equipmentID,
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        equipmentInfo(response.data);
        getAllDrivers(equipmentID);
        displayEquipmentDetails(true);
        displayTimeline(true);
        displayWebsiteSettings(true);
      })
      .catch((error) => {
        setWrongEquipmentID(true);
      });
  };

  const getAllDrivers = async (equipmentID) => {
    const token = localStorage.getItem('token');
    setAutocomplete(false);

    await axios
      .get(`/api/driver/${equipmentID}`, {
        equipmentID,
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setAllDrivers(response.data);
        setAutocomplete(true);
        setHastDrivers(true);
      })
      .catch((error) => {
        console.log(error);
        setAutocomplete(false);
      });
  };

  const testingField = (type, value) => {
    let regex;
    let res;
    switch (type) {
      case 'callerName':
        regex = new RegExp(/[a-z ,.'-]+$/i);
        res = regex.test(value);
        if (res === true) {
          setWrongCallerName(false);
          setCallerName(value);
        } else {
          setWrongCallerName(true);
        }
        return res;
      case 'customerName':
        regex = new RegExp(/[a-z ,.'-]+$/i);
        res = regex.test(value);
        if (res === true) {
          setWrongCustomerName(false);
          customer(value);
          displaySiteDetails(true);
        } else {
          setWrongCustomerName(true);
        }
        return res;
      case 'equipment_id':
        displayEquipmentDetails(false);
        displayTimeline(false);
        displayWebsiteSettings(false);
        setWrongEquipmentID(false);
        equipmentIDK(value);
        getEquipmentData(value);
        return res;
      default:
        return false;
    }
  };

  const asignDriverHistory = () => {
    setDriverHistory(!driverHistory);
    displayDriverHistory(!showDriverHistory);
  };

  const asignEvent = (type) => {
    let temp = false;
    if (type === 'Impact') {
      setImpacts(!impacts);
      const change = eventCategories;
      change.impacts = !eventCategories.impacts;
      setEventCategories(change);
      temp = !impacts || preop || sessions;
    } else if (type === 'Preop') {
      setPreop(!preop);
      const change = eventCategories;
      change.preop = !eventCategories.preop;
      setEventCategories(change);
      temp = impacts || !preop || sessions;
    } else if (type === 'Sessions') {
      setSessions(!sessions);
      const change = eventCategories;
      change.sessions = !eventCategories.sessions;
      setEventCategories(change);
      temp = impacts || preop || !sessions;
    }

    if (temp) {
      displayEvent(true);
    } else {
      displayEvent(false);
    }
  };

  const saveTicket = async () => {
    setSaveError(false);
    const token = localStorage.getItem('token');
    const data = {
      callerName,
      customerName,
      department,
      driverName: selectedDriver,
      equipment,
      eventTime,
      id: 0,
      location,
      saveDAte: new Date(Date.now()).toISOString(),
      status: 0,
      story,
      ticketId: 0,
      user: 0,
    };
    const header = { headers: { Authorization: `Bearer ${token}` } };

    await axios
      .post(`/api/ticket/new`, data, header)
      .then((response) => {
        setSaveError(false);
        setSavedTicket(true)
      })
      .catch((error) => {
        console.log(error);
        setSaveError(true);
      });
  };

  const checkSuggestions = (e) => {
    const check = list.filter(
      (suggestion) =>
        suggestion.toLowerCase().indexOf(selectedDriver.toLowerCase()) > -1
    );
    setFilteredSuggestions(check);
    setShowSuggestions(true);
  };

  const addLocationData = async () => {
    const token = localStorage.getItem('token');
    await axios
      .get(`/api/customer/details`, {
        params: { customer: customerName, equipmentId: equipment },
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        if (location === '' || department === '') {
          setLocation(response.data.location);
          setDepartment(response.data.department);
          setDisplayInput(true);
        }
        setRegion(response.data.region);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <Box title="Welcome to Collective Inteligence E-Helpdesk" width="auto">
      <GDContainer>
        <GDColumn>
          <GDColumnTitle>Incoming Customer Details</GDColumnTitle>
          <GDDataSection>
            <GDDataIdentifier>Caller’s Name*</GDDataIdentifier>
            {ticketInfo === 'default' || ticketInfo === null ? (
              <GDInput
                type="text"
                spellCheck={false}
                placeholder=""
                value={callerName}
                required
                aria-required="true"
                onBlur={(e) => {
                  testingField('callerName', e.target.value);
                }}
                onChange={(e) => {
                  setCallerName(e.target.value);
                  setSearch(false);
                }}
              />
            ) : (
              <GDTicketInfo>{ticketInfo.callerName}</GDTicketInfo>
            )}

            {wrongCallerName && ticketInfo === 'default' && (
              <GDInputError>Invalid caller's name</GDInputError>
            )}
          </GDDataSection>
          <GDDataSection>
            <GDDataIdentifier>Equipment ID*</GDDataIdentifier>{' '}
            {ticketInfo === 'default' || ticketInfo === null ? (
              <GDInput
                type="text"
                spellCheck={false}
                value={equipment}
                required
                placeholder=""
                onChange={(e) => {
                  equipmentIDK(e.target.value);
                  setSearch(false);
                }}
                onBlur={(e) => {
                  testingField('equipment_id', e.target.value);
                }}
              />
            ) : (
              <GDTicketInfo>{ticketInfo.equipment}</GDTicketInfo>
            )}
            {wrongEquipmentID && <GDInputError>Incorrect format</GDInputError>}
          </GDDataSection>
          <GDDataSection>
            <GDDataIdentifier>Customer Name</GDDataIdentifier>{' '}
            {ticketInfo === 'default' || ticketInfo === null ? (
              <GDInput
                type="text"
                spellCheck={false}
                placeholder=""
                value={customerName}
                aria-required="true"
                onChange={(e) => {
                  customer(e.target.value);
                  setSearch(false);
                }}
                onBlur={(e) => {
                  testingField('customerName', e.target.value);
                }}
              />
            ) : (
              <GDTicketInfo>{ticketInfo.customerName}</GDTicketInfo>
            )}
            {wrongCustomerName && (
              <GDInputError>Invalid customer's name</GDInputError>
            )}
          </GDDataSection>
          <GDDataSection>
            <GDDataIdentifier>Location Name</GDDataIdentifier>{' '}
            {ticketInfo === 'default' || ticketInfo === null ? (
              displayInput ? (
                <p>{location}</p>
              ) : (
                <GDInput
                  type="text"
                  spellCheck={false}
                  placeholder=""
                  value={location}
                  onChange={(e) => {
                    setLocation(e.target.value);
                    setSearch(false);
                  }}
                />
              )
            ) : (
              <GDTicketInfo>{ticketInfo.location}</GDTicketInfo>
            )}
          </GDDataSection>
          <GDDataSection>
            <GDDataIdentifier>Department Name</GDDataIdentifier>{' '}
            {ticketInfo === 'default' || ticketInfo === null ? (
              displayInput ? (
                <p>{department}</p>
              ) : (
                <GDInput
                  type="text"
                  spellCheck={false}
                  placeholder=""
                  value={department}
                  onChange={(e) => {
                    setDepartment(e.target.value);
                    setSearch(false);
                  }}
                />
              )
            ) : (
              <GDTicketInfo>{ticketInfo.department}</GDTicketInfo>
            )}
            {/* {displayInput ? (
              <GDInput type="text" spellCheck={false} placeholder="" />
            ) : (
              <p>No data was found</p>
            )} */}
          </GDDataSection>
          <GDDataSection>
            <GDDataIdentifier>Region</GDDataIdentifier> <p>{region}</p>
          </GDDataSection>
        </GDColumn>
        <GDColumn>
          <GDColumnTitle>Issue Details</GDColumnTitle>
          <GDDataSection>
            <GDIssueIdentifier>Story</GDIssueIdentifier>{' '}
            {ticketInfo === 'default' || ticketInfo === null ? (
              <GDStory
                value={story}
                onChange={(e) => {
                  setStory(e.target.value);
                  setSearch(false);
                }}
              />
            ) : (
              <GDTicketStory>{ticketInfo.story}</GDTicketStory>
            )}
          </GDDataSection>
          <GDDataSection>
            <GDIssueIdentifierTop>Categories</GDIssueIdentifierTop>
            <GDCheckboxColumn>
              <GDCheckboxContainerTop>
                <GDCheckboxHidden
                  checked={driverHistory}
                  onChange={() => {
                    setDriverHistory(!driverHistory);
              
                  }}
                />
                <GDCheckboxStyled
                  checked={driverHistory}
                  onChange={() => {
                    setDriverHistory(!driverHistory);
                  
                  }}
                  onClick={() => {asignDriverHistory(); setSearch(false)}}
                >
                  <FontAwesomeIcon
                    icon={faCheck}
                    checked={driverHistory}
                    className={
                      driverHistory
                        ? 'GDCheckboxIconSelected'
                        : 'GDCheckboxIcon'
                    }
                  />
                </GDCheckboxStyled>
                <GDCheckboxItem>Driver Access History</GDCheckboxItem>
              </GDCheckboxContainerTop>
              <GDCheckboxContainer>
                <GDCheckboxHidden
                  checked={impacts}
                  onChange={() => setImpacts(!impacts)}
                />
                <GDCheckboxStyled
                  checked={impacts}
                  onChange={() => {
                    setImpacts(!impacts);
                    setSearch(false);
                  }}
                  onClick={() => {asignEvent('Impact'); setSearch(false)}}
                >
                  <FontAwesomeIcon
                    icon={faCheck}
                    checked={impacts}
                    className={
                      impacts ? 'GDCheckboxIconSelected' : 'GDCheckboxIcon'
                    }
                  />
                </GDCheckboxStyled>
                <GDCheckboxItem>Impacts</GDCheckboxItem>
              </GDCheckboxContainer>
              <GDCheckboxContainer>
                <GDCheckboxHidden
                  checked={preop}
                  onChange={() => {
                    setPreop(!preop);
                    setSearch(false);
                  }}
                />
                <GDCheckboxStyled
                  checked={preop}
                  onChange={() => {
                    setPreop(!preop);
                    setSearch(false);
                  }}
                  onClick={() => {asignEvent('Preop'); setSearch(false)}}
                >
                  <FontAwesomeIcon
                    icon={faCheck}
                    checked={preop}
                    className={
                      preop ? 'GDCheckboxIconSelected' : 'GDCheckboxIcon'
                    }
                  />
                </GDCheckboxStyled>
                <GDCheckboxItem>Preop Check</GDCheckboxItem>
              </GDCheckboxContainer>
              <GDCheckboxContainer>
                <GDCheckboxHidden
                  checked={sessions}
                  onChange={() => setSessions(!sessions)}
                />
                <GDCheckboxStyled
                  checked={sessions}
                  onChange={() => setSessions(!sessions)}
                  onClick={() => {asignEvent('Sessions'); setSearch(false)}}
                >
                  <FontAwesomeIcon
                    icon={faCheck}
                    checked={sessions}
                    className={
                      sessions ? 'GDCheckboxIconSelected' : 'GDCheckboxIcon'
                    }
                  />
                </GDCheckboxStyled>
                <GDCheckboxItem>Sessions</GDCheckboxItem>
              </GDCheckboxContainer>
            </GDCheckboxColumn>
          </GDDataSection>
          <GDDataSection>
            <GDIssueIdentifier>Driver Name</GDIssueIdentifier>{' '}
            {ticketInfo === 'default' || ticketInfo === null ? (
              <div style={{ display: 'flex', flexFlow: 'column' }}>
                <GDInput
                  type="text"
                  spellCheck={false}
                  placeholder=""
                  value={selectedDriver}
                  onChange={(e) => {
                    setSelectedDriver(e.target.value);
                    checkSuggestions(e);
                  }}
                />
                {showSuggestions && selectedDriver !== '' && autocomplete && (
                  <GDAutocompleteContainer>
                    {filteredSuggestions.map((suggestion) => {
                      return (
                        <GDAutocomplete
                          onClick={(e) => {
                            setSelectedDriver(suggestion);
                            setShowSuggestions(false);
                          }}
                        >
                          {suggestion}
                        </GDAutocomplete>
                      );
                    })}
                  </GDAutocompleteContainer>
                )}
              </div>
            ) : (
              <GDInput
                type="text"
                spellCheck={false}
                placeholder=""
                value={ticketInfo.driverName}
                onChange={(e) => {
                  setSelectedDriver(e.target.value);
                }}
              />
            )}
            <GDIssueIdentifierExtra
              onClick={(e) => {
                setAlert(true);
              }}
            >
              Show all drivers
            </GDIssueIdentifierExtra>
            {alert && (
              <Alert type="All drivers" setAlert={setAlert}>
                <AllDrivers
                  drivers={allDrivers}
                  setSelectedDriver={setSelectedDriver}
                  setAlert={setAlert}
                />
              </Alert>
            )}
          </GDDataSection>
          <GDDataSection>
            <GDIssueIdentifier>Time</GDIssueIdentifier>{' '}
            {ticketInfo === 'default' || ticketInfo === null ? (
              <GDCalendar
                type="datetime-local"
                onChange={(e) => {
                  setEventTime(e.target.value);
                }}
              />
            ) : (
              <GDTicketCalendar>{ticketInfo.eventTime}</GDTicketCalendar>
            )}
          </GDDataSection>
          <GDRedButton
            onClick={(_) => {
              saveTicket();
            }}
          >
            Save
          </GDRedButton>
          { savedTicket && <Alert type="Saved Ticket" setAlert={setSavedTicket}>
            <GDNoCategoryContainer>
              <GDNoCategory>
                Ticket Successfully Saved
              </GDNoCategory>
              <GDButton
                onClick={(_) => {setSavedTicket(false)}}
              >
                Ok
              </GDButton>
            </GDNoCategoryContainer>
          </Alert>
          }
          <GDButton
            onClick={(e) => {
              setSearch(true);
              setCheckCategory(true);
              addLocationData();
            }}
            disabled={
              callerName === '' || customerName === '' || wrongEquipmentID
                ? true
                : false
            }
          >
            Search
          </GDButton>
          {saveError && (
            <GDSaveError>
              Please enter all the information required to save a ticket
            </GDSaveError>
          )}
        </GDColumn>
      </GDContainer>
      {search &&
        equipment !== '' &&
        !driverHistory &&
        !impacts &&
        !preop &&
        !sessions &&
        checkCategory && (
          <Alert type="Category notification" setAlert={setCheckCategory}>
            <GDNoCategoryContainer>
              <GDNoCategory>
                Please, pay attention that No category was selected. By default,
                in this case, information about all categories will be shown
              </GDNoCategory>
              <GDButton
                onClick={(_) => {
                  setDriverHistory(true);
                  displayDriverHistory(true);
                  setImpacts(true);
                  setPreop(true);
                  setSessions(true);
                  displayEvent(true);
                  setEventCategories({
                    impacts: true,
                    preop: true,
                    sessions: true,
                  });
                  setCheckCategory(false);
                }}
              >
                Ok
              </GDButton>
            </GDNoCategoryContainer>
          </Alert>
        )}
    </Box>
  );
}

export default GeneralDetails;

// FROM HERE JUST STYLES FOR THE COMPONENT

const GDContainer = styled.div`
  display: flex;
  margin: 2rem;
`;

const GDColumn = styled.div`
  width: 50%;
`;

const GDColumnTitle = styled.p`
  padding-bottom: 1rem;
`;

const GDDataSection = styled.div`
  display: flex;
  align-items: center;
  padding: 0.5rem 0;
`;

const GDDataIdentifier = styled.p`
  min-width: 150px;
`;

const GDIssueIdentifier = styled.p`
  min-width: 90px;
`;

const GDIssueIdentifierTop = styled.p`
  align-self: flex-start;
  min-width: 90px;
`;

const GDIssueIdentifierExtra = styled.p`
  margin-left: 10px;
  cursor: pointer;
`;

const GDInput = styled.input`
  width: 170px;
  height: 1.5rem;
  padding: 0 0.5rem;
  border: 1px solid #888888;
  color: #888888;
  &:focus {
    outline: none;
  }
`;

const GDTicketInfo = styled.polyline`
  padding: 0.25rem 0;
`;

const GDInputError = styled.p`
  margin-left: 0.5rem;
  color: red;
`;

const GDStory = styled.textarea`
  width: 158px;
  height: 2rem;
  margin: 0;
  padding: 1rem;
  border: 1px solid #888888;
  color: #888888;
  resize: none;
  &:focus {
    outline: none;
  }
`;

const GDTicketStory = styled.p`
  width: 158px;
  height: 2rem;
  margin: 0;
  padding: 1rem;
  border: 1px solid #888888;
`;

const GDCheckboxColumn = styled.div`
  display: flex;
  flex-flow: column;
  align-items: flex-start;
`;

const GDCheckboxContainerTop = styled.div`
  width: 220px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 0.5rem;
`;

const GDCheckboxContainer = styled.div`
  width: 220px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 0.5rem 0;
`;

const GDCheckboxHidden = styled.input`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const GDNoCategoryContainer = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  padding: 5rem 4rem;
`;

const GDNoCategory = styled.p`
  text-align: center;
`;

const GDCalendar = styled.input`
  width: 170px;
  height: 1.5rem;
  padding: 0 0.5rem;
  border: 1px solid #888888;
  color: #888888;
  &:focus {
    outline: none;
  }
`;

const GDTicketCalendar = styled.p`
  width: 170px;
  padding: 0.19rem 0.5rem;
  border: 1px solid #888888;
`;

const GDCheckboxStyled = styled.div`
  display: inline-block;
  width: 16px;
  height: 16px;
  text-align: center;
  background: ${(props) => (props.checked ? '#1d72e1' : 'transparent')};
  border: 1px solid #bbbbbb;
  border-radius: 3px;
  transition: all 150ms;
  cursor: pointer;
`;

const GDCheckboxItem = styled.span`
  margin-left: 10px;
`;

const GDAutocompleteContainer = styled.div`
  width: 184px;
  background-color: rgb(245, 245, 245);
  max-height: 177px;
  margin-top: 1.65rem;
  position: absolute;
  border: 1px solid #bbbbbb;
  overflow-y: scroll;
`;

const GDAutocomplete = styled.p`
  padding: 0.5rem 0.5rem;
  width: fit-content;
  :hover {
    cursor: pointer;
  }
`;

const GDRedButton = styled.button`
  width: fit-content;
  height: 2rem;
  cursor: pointer;
  margin-top: 1rem;
  margin-right: 1rem;
  padding: 0 2rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;

const GDButton = styled.button`
  width: fit-content;
  height: 2rem;
  cursor: pointer;
  margin-top: 1rem;
  padding: 0 2rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;

const GDSaveError = styled.p`
  margin-top: 0.5rem;
  color: red;
`;