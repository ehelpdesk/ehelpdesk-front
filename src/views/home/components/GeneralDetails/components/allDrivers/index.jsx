import React, { useState, useEffect } from "react";
import styled from "styled-components";

// AD = AllDrivers
function AllDrivers({ drivers, setSelectedDriver, setAlert }) {
  const [currentPage, setCurrentPage] = useState(1);
  const [currentDisplayed, setCurrentDisplayed] = useState([]);
  const [finalPage, setFinalPage] = useState(false);

  useEffect(() => {
    const getNames = (drivers) => {
      let totalDisplayed = 10;
      let final = totalDisplayed * currentPage - 1;
      let i = final - totalDisplayed + 1;
      let newArray = [];
      for (i; i <= final; i++) {
        if (i < drivers.length) {
          newArray.push(drivers[i].driver_name);
          setFinalPage(false);
        } else {
          setFinalPage(true);
        }
      }
      setCurrentDisplayed(newArray);
    };
    getNames(drivers);
  }, [currentPage, drivers]);

  return (
    <>
      {currentDisplayed.map((driver, index) => {
        return (
          <ADNames
            key={index}
            onClick={(_) => {
              setSelectedDriver(driver); setAlert(false);
            }}
          >
            {driver}
          </ADNames>
        );
      })}
      <ADButtonsSection>
        <ADButton
          onClick={(_) => {
            setCurrentPage(currentPage - 1);
          }}
          disabled={currentPage === 1 && true}
        >
          Previous
        </ADButton>
        <ADButton
          onClick={(_) => {
            setCurrentPage(currentPage + 1);
          }}
          disabled={finalPage}
        >
          Next
        </ADButton>
      </ADButtonsSection>
    </>
  );
}

export default AllDrivers;

// Styles of the component
const ADNames = styled.p`
  padding: 1rem 1rem 0.5rem 1rem;
  cursor: pointer;
  width: fit-content;
`;

const ADButtonsSection = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 1rem;
`;

const ADButton = styled.button`
  width: fit-content;
  height: 2rem;
  cursor: pointer;
  margin-top: 1rem;
  padding: 0 2rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;
