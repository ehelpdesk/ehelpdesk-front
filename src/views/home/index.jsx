import React, { useState, useEffect } from "react";
import GeneralDetails from "./components/GeneralDetails/";
import SiteDetails from "./components/SiteDetails";
import EquipmentDetails from "./components/EquipmentDetails";
import WebsiteSettings from "./components/WebsiteSetting";
import Timeline from "./components/Timeline";
import Event from "./components/Events";
import DriverHistory from "./components/DriverHistory";
import Alert from "../../generalComponents/alerts";
import axios from "axios";
import styled from "styled-components";

function Home() {
  const [viewTicket, setViewTicket] = useState("default");
  const [ticketInfo, setTicketInfo] = useState(null);
  const [expandSD, setExpandSD] = useState(false);
  const [expandED, setExpandED] = useState(false);
  const [expandTL, setExpandTL] = useState(false);
  const [expandWS, setExpandWS] = useState(false);
  const [expandEV, setExpandEV] = useState(false);
  const [expandDH, setExpandDH] = useState(false);
  const [alert, setAlert] = useState(false);
  const [alertType, setAlertType] = useState("");
  const [unresolvedTickets, setUnresolvedTickets] = useState([]);
  const [customer, setCustomer] = useState("");
  const [equipmentID, setEquipmentID] = useState("");
  const [equipmentInfo, setEquipmentInfo] = useState([]);
  const [displaySiteDetails, setDisplaySiteDetails] = useState(false);
  const [displayDriverHistory, setDisplayDriverHistory] = useState(false);
  const [displayEvent, setDisplayEvent] = useState(false);
  const [eventCategories, setEventCategories] = useState({
    impacts: false,
    preop: false,
    sessions: false,
  });
  const [displayWebsiteSettings, setDisplayWebsiteSettings] = useState(false);
  const [displayTimeline, setDisplayTimeline] = useState(false);
  const [displayEquipmentDetails, setDisplayEquipmentDetails] = useState(false);
  const [search, setSearch] = useState(false);
  const [clearForm, setClearForm] = useState(false);

  useEffect(() => {
    getGeneralDetails();
  }, [setUnresolvedTickets]);

  useEffect(() => {
    const displayTicketInfo = () => {
      let currentTicket;
      if (viewTicket !== "default") {
        currentTicket = unresolvedTickets.filter(
          (ticket) => ticket.id.toString() === viewTicket
        );
        setTicketInfo({
          id: currentTicket[0].id,
          ticketId: currentTicket[0].ticketId,
          callerName: currentTicket[0].callerName,
          customerName: currentTicket[0].customerName,
          equipment: currentTicket[0].equipment,
          location: currentTicket[0].location,
          department: currentTicket[0].department,
          story: currentTicket[0].story,
          eventTime: currentTicket[0].eventTime,
          saveDAte: currentTicket[0].saveDAte,
          driverName: currentTicket[0].driverName,
          user: currentTicket[0].user,
        });
        setSearch(false);
      } else {
        setTicketInfo("default");
        setSearch(false);
      }
    };
    displayTicketInfo();
  }, [viewTicket, unresolvedTickets]);

  const displayAlert = (type) => {
    setAlert(true);
    setAlertType(type);
  };

  const getGeneralDetails = async () => {
    const token = localStorage.getItem("token");

    await axios
      .get(`/api/ticket`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setUnresolvedTickets(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const clearTicket = () => {
    if (viewTicket === "default") {
      setClearForm(true);
    } else {
      setViewTicket("default");
      setClearForm(true);
    }
  };

  return (
    <>
      {alert && (
          <Alert type={alertType} setAlert={setAlert} ticketInfo={ticketInfo} />
        )
      }
      <HomeButtonsContainer>
        <form>
          <HomeHeaderLabel htmlFor="tickets">
            Unresolved tickets:
          </HomeHeaderLabel>
          <HomeHeaderSelect
            name="tickets"
            onChange={(e) => {
              setViewTicket(e.target.value);
            }}
            value={viewTicket}
          >
            <option value="default"></option>
            {unresolvedTickets.map((ticket, index) => {
              return (
                <option key={index} value={ticket.id}>
                  {" "}
                  Ticket #{ticket.id}
                </option>
              );
            })}
          </HomeHeaderSelect>
        </form>
        <HomeHeaderButtons onClick={(_) => clearTicket()}>
          Clear
        </HomeHeaderButtons>
        <HomeHeaderButtons
          onClick={(_) => displayAlert("resolve")}
          disabled={viewTicket === "default" ? true : false}
        >
          Resolve
        </HomeHeaderButtons>
        <HomeHeaderButtons
          onClick={(_) => displayAlert("escalate")}
          disabled={viewTicket === "default" ? true : false}
        >
          Escalate
        </HomeHeaderButtons>
      </HomeButtonsContainer>
      <GeneralDetails
        customer={setCustomer}
        customerName={customer}
        equipmentIDK={setEquipmentID}
        equipment={equipmentID}
        equipmentInfo={setEquipmentInfo}
        displaySiteDetails={setDisplaySiteDetails}
        displayEquipmentDetails={setDisplayEquipmentDetails}
        displayTimeline={setDisplayTimeline}
        displayDriverHistory={setDisplayDriverHistory}
        showDriverHistory={displayDriverHistory}
        displayEvent={setDisplayEvent}
        showEvent={displayEvent}
        eventCategories={eventCategories}
        setEventCategories={setEventCategories}
        displayWebsiteSettings={setDisplayWebsiteSettings}
        setSearch={setSearch}
        search={search}
        ticketInfo={ticketInfo}
        clearForm={clearForm}
        setClearForm={setClearForm}
      />
      {displaySiteDetails && search && equipmentID === "" && (
        <SiteDetails
          expand={expandSD}
          setExpand={setExpandSD}
          customer={customer}
          displaySiteDetails={setDisplaySiteDetails}
        />
      )}

      {displayEquipmentDetails && search && (
        <EquipmentDetails
          expand={expandED}
          setExpand={setExpandED}
          equipmentInfo={equipmentInfo}
        />
      )}
      {displayTimeline && search && (
        <Timeline
          expand={expandTL}
          setExpand={setExpandTL}
          equipmentID={equipmentID}
        />
      )}
      {displayWebsiteSettings && search && (
        <WebsiteSettings
          expand={expandWS}
          setExpand={setExpandWS}
          equipmentID={equipmentID}
          customer={customer}
        />
      )}
      {displayEvent && search && equipmentID !== "" && (
        <Event
          expand={expandEV}
          setExpand={setExpandEV}
          categories={eventCategories}
          equipmentID={equipmentID}
          customer={customer}
        />
      )}
      {displayDriverHistory && search && equipmentID !== "" && (
        <DriverHistory
          expand={expandDH}
          setExpand={setExpandDH}
          equipmentID={equipmentID}
        />
      )}
    </>
  );
}

export default Home;

// FROM HERE JUST STYLES FOR THE COMPONENT

const HomeButtonsContainer = styled.div`
  width: auto;
  margin: 3rem 4rem -1rem 4rem;
  display: flex;
  justify-content: flex-end;
`;

const HomeHeaderButtons = styled.button`
  width: fit-content;
  height: 2rem;
  cursor: pointer;
  margin: 0 0.25rem;
  padding: 0 1rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: transparent;
  &:focus {
    outline: none;
  }
`;

const HomeHeaderLabel = styled.label`
  padding-right: 0.5rem;
`;

const HomeHeaderSelect = styled.select`
  width: 110px;
  height: 2rem;
  cursor: pointer;
  margin: 0 0.25rem;
  padding-left: 0.5rem;
  border-radius: 4px;
  border: 1px solid #bbbbbb;
  background: white;
  &:focus {
    outline: none;
  }
`;